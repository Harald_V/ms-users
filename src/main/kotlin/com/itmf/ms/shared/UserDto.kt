package com.itmf.ms.shared

import java.io.Serializable

data class UserDto(
	var firstname: String = "",
	var lastname: String = "",
	var email: String = "",
	var password: String = "",
	var userId: String = "",
	var encryptedPassword: String = ""
) : Serializable {
	companion object {
		private const val serialVersionUID = -82L
	}
}
package com.itmf.ms.dal

import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface UserRepository : CrudRepository<UserEntity, Long> {

	fun findByEmail(email: String): UserEntity?
	fun findByUserId(userId: String): UserEntity?
}
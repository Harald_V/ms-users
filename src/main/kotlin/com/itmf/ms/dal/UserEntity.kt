package com.itmf.ms.dal

import org.hibernate.annotations.GenericGenerator
import java.io.Serializable
import javax.persistence.*

@Entity
@Table(name = "users")
data class UserEntity(
	@Id @GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	var id: String?,

	@Column
	var firstname: String,

	@Column
	var lastname: String,

	@Column(nullable = false, unique = true)
	var email: String,

	@Column(nullable = false, unique = true)
	var userId: String,

	@Column
	var encryptedPassword: String
) : Serializable {
	companion object {
		private const val serialVersionUID = -92L
	}

}
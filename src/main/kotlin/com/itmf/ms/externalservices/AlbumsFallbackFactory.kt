package com.itmf.ms.externalservices

import com.itmf.ms.models.AlbumResponseModel
import feign.FeignException
import feign.hystrix.FallbackFactory
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component

@Component
class AlbumsFallbackFactory : FallbackFactory<AlbumsServiceClient> {

	override fun create(cause: Throwable?): AlbumsServiceClient {
		return AlbumsServiceClientFallback(cause)
	}

}

class AlbumsServiceClientFallback(private val cause: Throwable?) : AlbumsServiceClient {

	private val log: Logger = LogManager.getLogger()

	override fun getAlbums(id: String): List<AlbumResponseModel> {
		if(cause is FeignException && cause.status() == HttpStatus.NOT_FOUND.value())
			log.error("404 when requesting AlbumsServiceClient with userID: $id. Cause: ${cause.localizedMessage}")
		else if(cause is FeignException) {
			log.error("feignException with userID: $id. Cause: ${cause.localizedMessage}")
		} else if(cause != null) {
			log.error("An error occured, with userID: $id. Cause: ${cause.localizedMessage}\"")
		}

		return emptyList()
	}

}
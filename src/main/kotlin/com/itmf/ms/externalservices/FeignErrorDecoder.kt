package com.itmf.ms.externalservices

import feign.Response
import feign.codec.ErrorDecoder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component
class FeignErrorDecoder : ErrorDecoder {

	@Autowired
	lateinit var env: Environment

	override fun decode(methodKEy: String, resp: Response): Exception {
		when(resp.status()) {

			400 -> return ResponseStatusException(HttpStatus.BAD_REQUEST, "")

			401 -> return ResponseStatusException(HttpStatus.UNAUTHORIZED, "")

			403 -> return ResponseStatusException(HttpStatus.FORBIDDEN, "Forbidden access.")

			404 -> return ResponseStatusException(HttpStatus.NOT_FOUND, "Users albums not found.")

		}
		return ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "An error occured.")
	}

}
package com.itmf.ms.externalservices

import com.itmf.ms.models.AlbumResponseModel
import feign.FeignException
import org.springframework.cloud.openfeign.FeignClient
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable

@FeignClient(name = "ms-albums", fallbackFactory = AlbumsFallbackFactory::class)
interface AlbumsServiceClient {

	@GetMapping("/users/{id}/albums")
	@ExceptionHandler(FeignException::class)
	fun getAlbums(@PathVariable("id") id: String): List<AlbumResponseModel>
}
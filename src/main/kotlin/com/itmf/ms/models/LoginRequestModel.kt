package com.itmf.ms.models

import org.jetbrains.annotations.NotNull
import javax.validation.constraints.Email
import javax.validation.constraints.Size

data class LoginRequestModel(

	@field:NotNull
	@field:Email
	var email: String = "",

	@field:NotNull
	@field:Size(min=8, message="not inf to 8")
	var password: String = ""
)
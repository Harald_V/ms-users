package com.itmf.ms.models

data class UserResponseModel(

	var firstname: String = "",

	var lastname: String = "",

	var email: String = "",

	//var password: String = "",

	var userID: String? = "",

	var albumList: List<AlbumResponseModel>? = ArrayList<AlbumResponseModel>()
)

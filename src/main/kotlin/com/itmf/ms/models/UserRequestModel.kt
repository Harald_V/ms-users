package com.itmf.ms.models

import org.jetbrains.annotations.NotNull
import javax.validation.Valid
import javax.validation.constraints.Email
import javax.validation.constraints.Size

data class UserRequestModel(


	@field:Size(min=2, message="not inf to 2")
	@field:NotNull
	var firstname: String = "",

	@Valid
	@NotNull
	@Size(min=2, message="not inf to 2")
	var lastname: String = "",

	@field:NotNull
	@field:Email
	var email: String = "",

	@field:NotNull
	@field:Size(min=8, message="not inf to 8")
	var password: String = "",

	var userId: String?

)
package com.itmf.ms.controllers

import com.google.gson.Gson
import com.itmf.ms.models.LoginRequestModel
import com.itmf.ms.models.UserRequestModel
import com.itmf.ms.models.UserResponseModel
import com.itmf.ms.services.UsersService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/users")
class UsersController {

	@Autowired
	lateinit var usersService: UsersService

	@Autowired
	lateinit var env: Environment

	@Autowired
	lateinit var gson: Gson

	@GetMapping("/status/check")
	fun status(): String? {
		return "working on " + env.getProperty("local.server.port") + " ,with token:" + env.getProperty("token.secret")
	}

	@PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
	@ResponseBody
	fun createUser(@Valid @RequestBody userRequestModel: UserRequestModel): ResponseEntity<UserResponseModel> {

		val userCreated = usersService.createUser(userRequestModel)

		return ResponseEntity(userCreated, HttpStatus.CREATED)
	}

	@PostMapping("/login", consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
	@ResponseBody
	fun login(@Valid @RequestBody LoginRequestModel: LoginRequestModel): ResponseEntity<String> {

		return ResponseEntity("ok", HttpStatus.ACCEPTED)
	}

	@GetMapping(
		value = ["/{userID}"],
		produces = [MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE]
	)
	fun getUser(@PathVariable("userID") userID: String): ResponseEntity<UserResponseModel> {

		val userResponseModel = usersService.getUserByUserID(userID)
		return ResponseEntity(userResponseModel, HttpStatus.FOUND)

	}

	@PostMapping("/jimmy", consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
	@ResponseBody
	fun jimmy(@Valid @RequestBody LoginRequestModel: LoginRequestModel): ResponseEntity<String> {

		println("yagayo")
		return ResponseEntity("Authentified!", HttpStatus.ACCEPTED)
	}

}
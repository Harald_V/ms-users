package com.itmf.ms

import com.itmf.ms.dal.UserRepository
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker
import org.springframework.cloud.client.discovery.EnableDiscoveryClient
import org.springframework.cloud.openfeign.EnableFeignClients
import org.springframework.data.jpa.repository.config.EnableJpaRepositories


@SpringBootApplication
@EnableDiscoveryClient //discovery
@EnableJpaRepositories(basePackageClasses = [UserRepository::class]) //jpa
@EnableFeignClients
@EnableCircuitBreaker
class UsersMsApplication

fun main(args: Array<String>) {
	runApplication<UsersMsApplication>(*args)
}





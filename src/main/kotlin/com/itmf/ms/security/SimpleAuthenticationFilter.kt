package com.itmf.ms.security

import com.fasterxml.jackson.databind.ObjectMapper
import com.itmf.ms.models.LoginRequestModel
import com.itmf.ms.services.UsersService
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.io.Decoders
import io.jsonwebtoken.security.Keys
import org.springframework.core.env.Environment
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken
import org.springframework.security.core.Authentication
import org.springframework.security.core.userdetails.User
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter
import java.security.Key
import java.util.*
import javax.servlet.FilterChain
import javax.servlet.http.HttpServletRequest
import javax.servlet.http.HttpServletResponse
import kotlin.collections.ArrayList

class SimpleAuthenticationFilter(private var env: Environment, private var userService: UsersService) :
	UsernamePasswordAuthenticationFilter() {


	override fun attemptAuthentication(request: HttpServletRequest, response: HttpServletResponse): Authentication {

		try {
			val loginReqModel: LoginRequestModel = ObjectMapper().readValue(request.inputStream, LoginRequestModel::class.java)

			val authentication = UsernamePasswordAuthenticationToken(
				loginReqModel.email,
				loginReqModel.password,
				ArrayList()
			)

			return authenticationManager.authenticate(authentication)

		} catch(ex: Exception) {
			//println("okkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk")
			ex.printStackTrace()
		}
		return super.attemptAuthentication(request, response)
	}

	override fun successfulAuthentication(
		request: HttpServletRequest,
		response: HttpServletResponse,
		chain: FilterChain,
		auth: Authentication
	) {
		val userName: String = (auth.principal as User).username
		val user = userService.getUserDetailsByEmail(userName)

		val keyBytes: ByteArray = Decoders.BASE64.decode(env.getProperty("token.secret"))
		val key: Key = Keys.hmacShaKeyFor(keyBytes)

		val token = Jwts.builder()
			.setSubject(user.userID)
			.setExpiration(Date(System.currentTimeMillis() + (env.getProperty("token.expiration.length")?.toLongOrNull() as Long)))
			.signWith(key)
			.compact()

		response.addHeader("token", token)
		response.addHeader("userId", user.userID)
	}
}



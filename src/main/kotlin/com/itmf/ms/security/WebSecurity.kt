package com.itmf.ms.security

import com.itmf.ms.services.UsersService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Configuration
import org.springframework.core.env.Environment
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder


@Configuration
@EnableWebSecurity
class WebSecurity : WebSecurityConfigurerAdapter() {

	lateinit var env: Environment

	@Autowired
	lateinit var userService: UsersService

	@Autowired
	lateinit var bCryptPasswordEncoder: BCryptPasswordEncoder

	@Autowired
	fun WebSecurity() {
		env = this.applicationContext.getBean(Environment::class.java)
	}

	/**
	 * NE PLUS TOUCHER !
	 */
	override fun configure(http: HttpSecurity) {

		http
			.addFilter(authenticationFilter())
			.authorizeRequests()
			//.antMatchers(HttpMethod.POST, "/users/login").permitAll()//fonctionne
			.anyRequest()
			.permitAll()
		//.authenticated()

		//.hasIpAddress(env.getProperty("gateway.ip"))//TODO find how!

		http.csrf().disable()
		http.headers().frameOptions().disable()
	}


	@Throws(Exception::class)
	fun authenticationFilter(): SimpleAuthenticationFilter? {
		val filter = SimpleAuthenticationFilter(env, userService)
		filter.setAuthenticationManager(authenticationManagerBean())
		//filter.setAuthenticationFailureHandler(loginFailureHandler())
		filter.setFilterProcessesUrl(env.getProperty("login.url"))
		return filter
	}

	override fun configure(auth: AuthenticationManagerBuilder?) {
		auth?.userDetailsService(userService)?.passwordEncoder(bCryptPasswordEncoder)
	}

}
package com.itmf.ms.services

import com.itmf.ms.models.UserRequestModel
import com.itmf.ms.models.UserResponseModel
import org.springframework.security.core.userdetails.UserDetailsService


interface UsersService : UserDetailsService {

	fun createUser(user: UserRequestModel): UserResponseModel

	//from UserDetailsService
	fun getUserDetailsByEmail(email: String): UserResponseModel

	fun getUserByUserID(userID: String): UserResponseModel
}
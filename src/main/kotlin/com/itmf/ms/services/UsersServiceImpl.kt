package com.itmf.ms.services

import com.itmf.ms.dal.UserEntity
import com.itmf.ms.dal.UserRepository
import com.itmf.ms.externalservices.AlbumsServiceClient
import com.itmf.ms.models.UserRequestModel
import com.itmf.ms.models.UserResponseModel
import feign.FeignException
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import org.modelmapper.ModelMapper
import org.modelmapper.convention.MatchingStrategies
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.core.env.Environment
import org.springframework.security.core.userdetails.User
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
@Transactional
class UsersServiceImpl : UsersService {

	@Autowired
	lateinit var userRepository: UserRepository

	@Autowired
	lateinit var env: Environment

	/*
	@Autowired
	lateinit var restTemplate: RestTemplate
*/
	@Autowired
	lateinit var albumsServiceClient: AlbumsServiceClient

	@Autowired
	lateinit var bCryptPasswordEncoder: BCryptPasswordEncoder

	private val log: Logger = LogManager.getLogger()

	override fun createUser(user: UserRequestModel): UserResponseModel {
		user.userId = UUID.randomUUID().toString()

		val modelMapper = ModelMapper()
		modelMapper.configuration.isSkipNullEnabled = true
		modelMapper.configuration.matchingStrategy = MatchingStrategies.STRICT

		val userEntity: UserEntity = modelMapper.map(user, UserEntity::class.java)
		userEntity.encryptedPassword = bCryptPasswordEncoder.encode(user.password).toString()

		val userPersisted = userRepository.save(userEntity)

		return modelMapper.map(userPersisted, UserResponseModel::class.java)
	}

	override fun getUserDetailsByEmail(email: String): UserResponseModel {
		val user: UserEntity? = userRepository.findByEmail(email) ?: throw UsernameNotFoundException(email)
		return ModelMapper().map(user, UserResponseModel::class.java)
	}

	override fun getUserByUserID(userID: String): UserResponseModel {
		val user: UserEntity? = userRepository.findByUserId(userID) ?: throw UsernameNotFoundException(userID)

		val userModel = ModelMapper().map(user, UserResponseModel::class.java)

		log.info("before albumsServiceClient.getAlbums")

		val albumList = albumsServiceClient.getAlbums(userID)
		userModel.albumList = albumList

		return userModel
	}

	//from userDetails interface
	override fun loadUserByUsername(username: String): UserDetails {
		val user: UserEntity? = userRepository.findByEmail(username) ?: throw UsernameNotFoundException(username)
		return User(user?.email, user?.encryptedPassword, true, true, true, true, ArrayList())
	}
} 
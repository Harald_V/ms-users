package com.itmf.ms

import feign.Logger
import org.springframework.cloud.client.loadbalancer.LoadBalanced
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.web.client.RestTemplate

@Configuration
class Config {

	@Bean
	@LoadBalanced
	fun restTemplate(): RestTemplate {
		return RestTemplate()
	}

	@Bean
	fun bCryptPasswordEncoder(): BCryptPasswordEncoder {
		return BCryptPasswordEncoder()
	}

	@Bean
	fun feignLoggerLevel(): Logger.Level {
		return Logger.Level.FULL
	}

}